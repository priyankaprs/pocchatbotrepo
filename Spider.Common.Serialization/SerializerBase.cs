﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Spider.Common.Serialization
{
    public abstract class SerializerBase<TTargetType>
        where TTargetType : new()
    {
        #region Protected Members

        protected readonly Type _type;
        protected readonly Dictionary<int, TextFieldAttribute> _textFields = new Dictionary<int, TextFieldAttribute>();

        #endregion

        #region Construction

        protected SerializerBase()
        {
            // Get the Reflection type for the Generic Argument
            _type = GetType().GetGenericArguments()[0];

            // Double check that the TextSerializableAttribute has been attached to the TargetType
            object[] serAttrs = _type.GetCustomAttributes( typeof( TextSerializableAttribute ), false );
            if ( serAttrs.Length == 0 )
                throw new TextSerializationException( "TargetType must have a TextSerializableAttribute attached" );

            // Get all the public properties and fields on the class
            MemberInfo[] members = _type.GetMembers( BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetField | BindingFlags.GetProperty );
            foreach ( MemberInfo member in members )
            {
                // Check to see if they've marked up this field/property with the attribute for serialization
                object[] fieldAttrs = member.GetCustomAttributes( typeof( TextFieldAttribute ), false );
                if ( fieldAttrs.Length > 0 )
                {
                    TextFieldAttribute textField = (TextFieldAttribute)fieldAttrs[0];
                    textField.Member = member;

                    // If they don't override the name in the Attribute, use the MemberInfo name
                    if (string.IsNullOrEmpty(textField.Name))
                        textField.Name = member.Name;

                    // Check for the AllowedValues Attribute and if it's there, store away the values into the other holder attribute
                    object[] allowedAttrs = member.GetCustomAttributes( typeof( AllowedValuesAttribute ), false );
                    if ( allowedAttrs.Length > 0 )
                    {
                        AllowedValuesAttribute allowedAttr = (AllowedValuesAttribute)allowedAttrs[0];
                        textField.AllowedValues = allowedAttr.AllowedValues;
                    }

                    _textFields.Add( textField.Position, textField );
                }
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>Creates a single TargetType object given a record string.</summary>
        /// <param name="parseList">Individual fields to be assigned to this object</param>
        /// <param name="returnMaybe">Possible return object. Allows creation in calling program.</param>
        /// <returns>Newly created TargetType object.</returns>
        /// <exception cref="ArgumentNullException">Thrown if text is null.</exception>
        /// <exception cref="TextSerializationException">Thrown if the number of fields in the record doesn't match that in TargetType.</exception>
        protected TTargetType Deserialize(List<string> parseList , TTargetType returnMaybe = default(TTargetType))
        {
            if (parseList == null)
                throw new ArgumentNullException("parseList", "parseList cannot be null");

            // Create the correct return objects depending on whether this is a value or reference type
            // This makes a difference for reflection later on when populating the fields dynamically.
            TTargetType returnObj = returnMaybe == null || returnMaybe.Equals(default(TTargetType)) ? new TTargetType() : returnMaybe;
            ValueType returnStruct = null;
            if (_type.IsValueType)
            {
                object tempObj = returnObj;
                returnStruct = (ValueType)tempObj;
            }

            int requiredCount = _textFields.Values.Count(attr => !attr.Optional);
            if (parseList.Count < requiredCount || parseList.Count > _textFields.Count)
                throw new TextSerializationException("TargetType field count doesn't match number of items in text");

            // For each field that is parsed in the string, populate the correct corresponding field in the TargetType
            TextFieldAttribute[] attributes = _textFields.Values.ToArray();
            attributes = attributes.OrderBy(i => i.Position).ToArray();
            for (int i = 0; i < parseList.Count; i++)
            {
                TextFieldAttribute attr = attributes[i];
                if (attr != null)
                {
                    object fieldObj;
                    string strVal = Truncate(parseList[i], attr.Size);

                    // If there is a custom formatter, then use that to deserialize the string, otherwise use the default
                    // .NET behvavior.
                    if (attr.Formatter != null)
                        fieldObj = attr.Formatter.Deserialize(strVal);
                    else
                    {
                        // Handle Null or Empty String
                        if (string.IsNullOrEmpty(strVal) && attr.NativeType != typeof(string) && IsNullable(attr.NativeType))
                            fieldObj = null;
                        else
                        {
                            Type underlyingType = Nullable.GetUnderlyingType(attr.NativeType) ?? attr.NativeType;
                            fieldObj = Convert.ChangeType(strVal, underlyingType);
                        }
                    }

                    // Depending on whether the TargetType is a class or struct, you have to populate the fields differently
                    if (_type.IsValueType)
                        AssignToStruct(returnStruct, fieldObj, attr.Member);
                    else
                        AssignToClass(returnObj, fieldObj, attr.Member);
                }
            }

            // If this was a value type, need to do a little more magic so can be returned properly
            if (_type.IsValueType)
            {
                object tempObj = (object)returnStruct;
                returnObj = (TTargetType)tempObj;
            }

            return returnObj;
        }

        private static bool IsNullable(Type type)
        {
            if (!type.IsValueType)  // Reference Type
                return true;
            else if (Nullable.GetUnderlyingType(type) != null)  // Nullable<T>
                return true;
            else    // Value Type
                return false;
        }

        protected static string Truncate(string str, int length)
        {
            if (length > -1 && str.Length > length)
                str = str.Substring(0, length);
            return str;
        }

        protected static void AssignToClass(object obj, object val, MemberInfo member)
        {
            if (member is PropertyInfo)
                ((PropertyInfo)member).SetValue(obj, val, null);
            else if (member is FieldInfo)
                ((FieldInfo)member).SetValue(obj, val);
            else
                throw new TextSerializationException("Invalid MemberInfo type encountered");
        }

        protected static void AssignToStruct(ValueType obj, object val, MemberInfo member)
        {
            if (member is PropertyInfo)
                ((PropertyInfo)member).SetValue(obj, val, null);
            else if (member is FieldInfo)
                ((FieldInfo)member).SetValue(obj, val);
            else
                throw new TextSerializationException("Invalid MemberInfo type encountered");
        }

        #endregion
    }
}