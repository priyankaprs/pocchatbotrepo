using System;

namespace Spider.Common.Serialization
{
    [AttributeUsage( AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false )]
    public class TextSerializableAttribute : Attribute
    {
        public TextSerializableAttribute()
        { }
    }
}