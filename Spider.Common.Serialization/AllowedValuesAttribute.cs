#region Using Directives

using System;
using System.Collections.Generic;
using System.Text;

#endregion

namespace Spider.Common.Serialization
{
    /// <summary>Defines the allowed characters that can be used for a field in the file.</summary>
    class AllowedValuesAttribute : Attribute
    {
        #region Private Variables

        private object[] _allowedValues;

        #endregion

        #region Construction

        /// <summary>Default Constructor.</summary>
        /// <param name="allowedValues">Array of characters for each value that is permitted.</param>
        public AllowedValuesAttribute( params object[] allowedValues )
        {
            _allowedValues = allowedValues;
        }

        #endregion

        #region Public Properties

        /// <summary>Array of characters for each value that is permitted.</summary>
        public object[] AllowedValues
        {
            get { return _allowedValues;  }
            set { _allowedValues = value; }
        }

        #endregion
    }
}