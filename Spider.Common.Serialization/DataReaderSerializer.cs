﻿using System;
using System.Collections.Generic;
using System.Data.Common;

namespace Spider.Common.Serialization
{
    public class DataReaderSerializer<TTargetType> : SerializerBase<TTargetType>
        where TTargetType : new()
    {
        public TTargetType Deserialize(DbDataReader reader, TTargetType returnMaybe = default(TTargetType))
        {
            if (reader == null)
                throw new ArgumentNullException("reader", "reader cannot be null");

            // Parse the record into it's individual fields depending on the type of serializer this is
            List<string> parseList = new List<string>(reader.FieldCount);
            for (int i = 0; i < reader.FieldCount; i++)
                parseList.Add(reader[i].ToString());

            return Deserialize(parseList, returnMaybe);
        }

        /// <summary>Deserializes the contents of a Data Reader into an array of Target Type</summary>
        /// <param name="reader">An already open DbDataReader containing the elements to deserialize</param>
        /// <param name="count">The number of records to deserialize. If count == -1, then all records will be deserialized</param>
        public ICollection<TTargetType> DeserializeArray(DbDataReader reader, int count = -1)
        {
            List<TTargetType> returnList = new List<TTargetType>();
            int recordsLeft = count;

            while (reader.Read())
            {
                returnList.Add(Deserialize(reader));
                recordsLeft--;

                // If -1 passed in, then this will never equal zero, and this will not
                // be used as an exit condition, which is the point
                if (recordsLeft == 0)
                    break;
            }

            return returnList;
        }

        public IEnumerable<TTargetType> DeserializeEnumerable(DbDataReader reader)
        {
            while(reader.Read())
            {
                yield return Deserialize(reader);
            }
        }
    }
}