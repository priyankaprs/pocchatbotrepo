﻿using Spider.Common.Serialization;
using System.Collections.Generic;

namespace Bot_Application_try1.Models
{
    [TextSerializable]
    public class ApplicantRecord
    {
        [TextField(0)]
        public string Name { get; set; }
        [TextField(1)]
        public string PreferredLocation { get; set; }
        [TextField(2)]
        public List<string> AreasOfExpertise { get; set; }
        [TextField(3)]
        public int? YearsOfExperience { get; set; }
        [TextField(4)]
        public string NoticePeriod { get; set; }
        [TextField(5)]
        public string ContactNumber { get; set; }
        [TextField(6)]
        public string EmailAddress { get; set; }
        [TextField(7)]
        public string FileName { get; set; }
    }
}