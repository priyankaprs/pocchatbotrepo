﻿using Microsoft.Bot.Builder.FormFlow;
using System;
using System.Collections.Generic;

namespace Bot_Application_try1.Models
{
    public enum Location
    {
        Pune,
        Bangalore,
        US
    }
    public enum Expertise
    {
        Technology,
        DotNet,
        Java,
        Android,
        IOS,
        Chatbot
    }
    public enum TermOfNotice
    {
        OneMonth=1,
        TwoMonths,
        ThreeMonths,
        MoreThanSixMonths,
        NotApplication
    };
    [Serializable]
    public class ApplicantDetails
    {
        [Pattern(@"([A-Za-z]+)\s+([A-Za-z]+)")]
        [Prompt("<h1>Waht's your name ?</h1>")]
        public string Name { get; set; }
        public Location? PreferredLocation { get; set; }
        //[Prompt("Experties in:  { && }", ChoiceStyle = ChoiceStyleOptions.Buttons)]
        public List<Expertise> AreasOfExpertise { get; set; }
        public int? YearsOfExperience { get; set; }
        public TermOfNotice NoticePeriod { get; set; }
        [Optional]
        [Pattern(@"^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$")]
        public string ContactNumber { get; set; }
        [Pattern(@"[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")]
        public string EmailAddress { get; set; }
        public static IForm<ApplicantDetails> BuildForm()
        {
            return new FormBuilder<ApplicantDetails>().Message("please fill below details ( Type 'Quit' to exit from the application form )..")
                             .Build();
        }
    }
}