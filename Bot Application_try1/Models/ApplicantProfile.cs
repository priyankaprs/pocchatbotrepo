﻿using System;

namespace Bot_Application_try1.Models
{
    [Serializable]
    public class ApplicantProfile
    {
        public ApplicantDetails ApplicantDetails { get; set; }
        public string FileName { get; set; }
        public byte[] File { get; set; }
    }
}