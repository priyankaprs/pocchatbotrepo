﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using Bot_Application_try1.Models;

namespace Bot_Application_try1.Dialogs
{
    [Serializable]
    public class ReceiveAttachmentDialog : IDialog<object>
    {
        private ApplicantProfile applicantProfile;

        public ReceiveAttachmentDialog(ApplicantProfile applicantProfile)
        {
            this.applicantProfile = applicantProfile;
        }

        public async Task StartAsync(IDialogContext context)
        {
            context.PostAsync("Please attach resume:");
            context.Wait(this.MessageReceivedAsync);
        }

        public virtual async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> argument)
        {
            var message = await argument;
            if (message.Attachments != null && message.Attachments.Any())
            {
                try
                {
                    var attachment = message.Attachments.First();
                    using (HttpClient httpClient = new HttpClient())
                    {
                        // Skype & MS Teams attachment URLs are secured by a JwtToken, so we need to pass the token from our bot.
                        if ((message.ChannelId.Equals("skype", StringComparison.InvariantCultureIgnoreCase) || message.ChannelId.Equals("msteams", StringComparison.InvariantCultureIgnoreCase))
                            && new Uri(attachment.ContentUrl).Host.EndsWith("skype.com"))
                        {
                            var token = await new MicrosoftAppCredentials().GetTokenAsync();
                            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                        }
                        var fileAsByteArray = await httpClient.GetByteArrayAsync(attachment.ContentUrl);
                        applicantProfile.FileName = attachment.Name;
                        applicantProfile.File = fileAsByteArray;
                        context.Done(applicantProfile);

                    }
                }
                catch (Exception e)
                {
                   await context.PostAsync("Issue in attaching resume :" + e);
                }
                
            }
            else
            {
                await context.PostAsync("Please attach file!");
            }
        }
    }
}