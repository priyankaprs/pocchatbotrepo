﻿using Bot_Application_try1.Models;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using System.Linq;
using System.Threading.Tasks;

namespace Bot_Application_try1.Dialogs
{
    public class JobApplicationDialog
    {

        public static readonly IDialog<string> dialog = Chain.PostToChain()
            .Select(i => i.Text)
            
            .Switch(new RegexCase<IDialog<string>>(new System.Text.RegularExpressions
                .Regex("^hi", System.Text.RegularExpressions.RegexOptions.IgnoreCase),
                (context, text) =>
                {
                    return Chain.ContinueWith(new GreetingDialog(), AfterGreetingContinuation);
                }
                ),

            new RegexCase<IDialog<string>>(new System.Text.RegularExpressions
                .Regex("^yes", System.Text.RegularExpressions.RegexOptions.IgnoreCase),
                (context, text) =>
                {
                    return Chain.ContinueWith(FormDialog.FromForm(ApplicantDetails.BuildForm,
                   FormOptions.PromptInStart), AfterApplicationContinuation);
                }
                )
            , new DefaultCase<string, IDialog<string>>((context, text) =>
             {
                 return Chain.ContinueWith(new GreetingDialog(), AfterNoContinuation);
             })
            //,new DefaultCase<string, IDialog<string>>((context, text) =>
            // {
            //     return Chain.ContinueWith(FormDialog.FromForm(ApplicantDetails.BuildForm,
            //         FormOptions.PromptInStart), AfterNoContinuation);
            // })
            )
            .Unwrap()
            .PostToUser();

        private async static Task<IDialog<string>> AfterApplicationContinuation(IBotContext context, IAwaitable<object> item)
        {
            var message = await item;
            var name = string.Empty;
            context.UserData.TryGetValue<string>("Name", out name);
            return Chain.Return($"Thank you {name} for your application. We will revert you back soon !");
        }
        private async static Task<IDialog<string>> AfterNoContinuation(IBotContext context, IAwaitable<object> item)
        {
            var message = await item;
            var name = string.Empty;
            context.UserData.TryGetValue<string>("Name", out name);
            return Chain.Return($"Thank you {name} for visiting our portal!");
        }

        private async static Task<IDialog<string>> AfterGreetingContinuation(IBotContext context, IAwaitable<object> item)
        {
            //This till capture entire ApplicationDetails object after form is completed.
            var message = await item;
            var name = string.Empty;
            context.UserData.TryGetValue<string>("Name", out name);
            return Chain.Return($"Do you want to apply for a job?");
        }
    }
}