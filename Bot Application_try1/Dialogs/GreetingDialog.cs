﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace Bot_Application_try1.Dialogs
{
    [Serializable]
    public class GreetingDialog : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            //await context.PostAsync("Welcome to Spiderlogic !");
            //await Respond(context);
            context.Wait(MessageReceivedAsync);
            
        }

        public async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            //var name = string.Empty;
            //var getName = false;
            ////var canProceed = false;

            //context.UserData.TryGetValue<string>("Name", out name);
            //context.UserData.TryGetValue<bool>("GetName", out getName);
            ////context.UserData.TryGetValue<bool>("CanProceed", out canProceed);

            //if (getName)
            //{
            //    name = message.Text;
            //    context.UserData.SetValue<string>("Name", name);
            //    context.UserData.SetValue<bool>("GetName", false);
            //}  

            // await Respond(context);
            await context.PostAsync($"Hello, How can I help you?");
           context.Done(message);
        }

        private static async Task Respond(IDialogContext context)
        {
            var name = string.Empty;
            //var canProceed = false;
            context.UserData.TryGetValue<string>("Name", out name);
            if (string.IsNullOrEmpty(name))
            {
                await context.PostAsync("What is your name?");
                context.UserData.SetValue<bool>("GetName", true);
            }
            else
            {
                await context.PostAsync($"Hello {name} , How can I help you?");
            }
        }
    }
}