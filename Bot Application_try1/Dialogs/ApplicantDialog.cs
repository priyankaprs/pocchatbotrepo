﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Connector;
using Bot_Application_try1.Models;

namespace Bot_Application_try1.Dialogs
{
    [Serializable]
    public class ApplicantDialog : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            //await context.PostAsync("<form action=''>< input type = 'checkbox' name = 'vehicle' value = 'Bike' > I have a bike<br>< input type = 'checkbox' name = 'vehicle' value = 'Car' > I have a car</form>");
            context.Wait(this.MessageRecievedAsync);
        }

        private async Task MessageRecievedAsync(IDialogContext context, IAwaitable<object> result)
        {
            //context.PostAsync("Candidate Dialog MessageRecievedAsync");
            var message = await result as Activity;
            var candidateDetailsFormDialog = FormDialog.FromForm(BuildCandidateDetailsForm);
            context.Call(candidateDetailsFormDialog,this.ResumeAfterCandidateFormDialog);     
        }

        private async Task ResumeAfterCandidateFormDialog(IDialogContext context, IAwaitable<ApplicantDetails> result)
        {
            var candidateDetail = await result as ApplicantDetails;
            context.Done(candidateDetail);
        }

        private IForm<ApplicantDetails> BuildCandidateDetailsForm()
        {
            return new FormBuilder<ApplicantDetails>()
                .Build();

        }
    }
}