﻿using Bot_Application_try1.Helper;
using Bot_Application_try1.Models;
using Bot_Application_try1.Utilities;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Bot_Application_try1.Dialogs
{
    [Serializable]
    [LuisModel("7743b938-c8f0-4735-8632-d8e0edbf5fb8", "cb7f174434a4424ea0d8dd65dac9aec6")]
    public class LUISDialog :LuisDialog<ApplicantDetails>
    {
        private ApplicantProfile applicantProfile;
        private readonly BuildFormDelegate<ApplicantDetails> Application;
        private const string FileUploadLocation = "D:\\AttachedFiles\\";
        public LUISDialog()
        {
            this.applicantProfile = new ApplicantProfile();
        }

        //public LUISDialog(BuildFormDelegate<ApplicantDetails> application)
        //{
        //    this.Application = application;
        //}

        [LuisIntent("")]
        public async Task None(IDialogContext context , LuisResult result)
        {
            await context.PostAsync("I am sorry, I dont know what you mean.");
            context.Wait(MessageReceived);
        }
        [LuisIntent("Greeting")]
        public async Task Greeting(IDialogContext context, LuisResult result)
        {
            var thankYouFlag = false;
            foreach (var entity in result.Entities.Where(Entity => Entity.Type == "Greeting"))
            {
                var value = entity.Entity.ToLower();
                if (value == "thank you" || value == "thanks" || value == "bye" || value == "yes")
                {
                    await context.PostAsync("Thank you for visiting our portal :)");
                    thankYouFlag = true;
                    context.Wait(MessageReceived);
                    return;
                }
            }
           await context.PostAsync("Hello, how can I help you?");
           context.Wait(MessageReceived);
           return;
          //  context.Call(new GreetingDialog(), Callback);
        }

        private async Task Callback(IDialogContext context, IAwaitable<object> result)
        {
            context.Wait(MessageReceived);
        }

        [LuisIntent("JobApplication")]
        public async Task JobApplication(IDialogContext context, LuisResult result)
        {
            await context.PostAsync("Please fill below details ( Type 'Quit' to exit from the application form )..");
            //await context.PostAsync("<form action=''><input type='checkbox' name='vehicle' value='Bike'>I have a bike<br><input type='checkbox' name='vehicle' value='Car'>I have a car</form>");
            var applicantDetailsFormDialog = FormDialog.FromForm(BuildApplicantDetailsForm,FormOptions.PromptInStart);
            context.Call(applicantDetailsFormDialog, ResumeAfterApplicantFormDialog);
        }

        private async Task ResumeAfterApplicantFormDialog(IDialogContext context, IAwaitable<object> result)
        {
            var applicantDetail = await result as ApplicantDetails;
            applicantProfile.ApplicantDetails = applicantDetail;
            context.Call(new ReceiveAttachmentDialog(applicantProfile), ResumeAfterReceiveAttachmentDialog);
            //context.Done(applicantDetail);
        }

        public IForm<ApplicantDetails> BuildApplicantDetailsForm()
        {
            return new FormBuilder<ApplicantDetails>()
                .Build();
        }

        //public async Task BuildFormUsingAdaptiveCards(Activity message,ConnectorClient connector)
        //{
        //    Activity replyToConversation = message.CreateReply("Should go to conversation");
        //    replyToConversation.Attachments = new List<Attachment>();

        //    List<CardAction> cardButtons = new List<CardAction>();

        //    CardAction plButton = new CardAction()
        //    {
        //        Value = $"https://<OAuthSignInURL",
        //        Type = "signin",
        //        Title = "Connect"
        //    };
        //    var adaptiveCards = new AdaptiveCards.AdaptiveCard()
        //    {
        //        Body = new List<AdaptiveCards.CardElement>() {
        //            new AdaptiveCards.TextBlock() { Text = "Please select your area of expertise from belw options." },
        //            new AdaptiveCards.ChoiceSet(){ Id="AreaOfExpertise" ,
        //                Choices = new List<AdaptiveCards.Choice>(){
        //                    new AdaptiveCards.Choice() { IsSelected = false,Speak = "DotNet",Title ="DotNet",Value="DotNet" },
        //                    new AdaptiveCards.Choice() { IsSelected = false, Speak = "Java",Title= "Java",Value="Java" },
        //                    new AdaptiveCards.Choice() { IsSelected = false, Speak = "Android",Title= "Android",Value="Android" },
        //                    new AdaptiveCards.Choice() { IsSelected = false, Speak = "IOS",Title= "IOS",Value="IOS" },
        //                    new AdaptiveCards.Choice() { IsSelected = false, Speak = "ChatBot",Title= "ChatBot",Value="ChatBot" }
        //                },IsMultiSelect=true }}
        //    };

        //    cardButtons.Add(plButton);

        //    //SigninCard plCard = new SigninCard("You need to authorize me", new List<CardAction>() { plButton });

        //    //Attachment plAttachment = adaptiveCards.a;
        //    //replyToConversation.Attachments.Add(adaptiveCards);

        //    await connector.Conversations.SendToConversationAsync(replyToConversation);
        //}

        private  async Task ResumeAfterReceiveAttachmentDialog(IDialogContext context, IAwaitable<object> result)
        {
            var applicantProfile = await result as ApplicantProfile;
            try
            {
                this.MiscellaneousTask(applicantProfile);
                await context.PostAsync("Resume uploaded successfully!");
                context.PostAsync("Your application is under progress. We will revert back to you.");
                context.EndConversation("End of conversation!");
            }
            catch (Exception e)
            {
                context.PostAsync("We are facing problem in saving your application :"+e);
                return;
            }           
            // context.Done(message);
        }

        private void MiscellaneousTask(ApplicantProfile message)
        {
            var applicantRecord = CsvMapper.Map(applicantProfile);
            var applicantRecords = new List<ApplicantRecord>
                {
                    applicantRecord
                };
            CsvWriter.WriteRecordsToFile(applicantRecords);

            FileUtils.SaveBytesToFile(Path.Combine(ConfigurationManager.AppSettings["OutputDirectory"] ,applicantRecord.FileName), message.File);
        }
        
        [LuisIntent("JobOpenings")]
        public async Task JobOpening(IDialogContext context, LuisResult result)
        {
            foreach( var entity in result.Entities.Where(Entity=> Entity.Type == "Application"))
            {
                var value = entity.Entity.ToLower();
                if(value == "java" || value == "android" || value == "c #" || value == "chatbot")
                {
                    await context.PostAsync("Yes we have that. What action would you like to perform?");
                    context.Wait(MessageReceived);
                    return;
                }
                else if(value == "opening" || value == "vacancy" || value=="vacancies")
                {
                    await context.PostAsync("Please provide which technology you are searching for...");
                    context.Wait(MessageReceived);
                    return;
                }
                else
                {
                    await context.PostAsync("Sorry, No opening as of now..!");
                    context.Wait(MessageReceived);
                    return;
                }
            }
            await context.PostAsync("I am sorry we dont have that..!");
            context.Wait(MessageReceived);
            return;
        }
    }
} 