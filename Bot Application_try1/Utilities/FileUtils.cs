﻿using Chronic;
using System.IO;

namespace Bot_Application_try1.Utilities
{
    public class FileUtils
    {
        public static void SaveBytesToFile(string filename, byte[] bytesToWrite)
        {
            if (filename != null && filename.Length > 0 && bytesToWrite != null)
            {
                filename = ReplaceSpecialChars(filename);
                if (!Directory.Exists(Path.GetDirectoryName(filename)))
                    Directory.CreateDirectory(Path.GetDirectoryName(filename));

                FileStream file = File.Create(filename);

                file.Write(bytesToWrite, 0, bytesToWrite.Length);

                file.Close();
            }
        }

        private static string ReplaceSpecialChars(string val)
        {
            const string specialChars = "~!@$%^()_+`-={}[]',\"#&";
            specialChars.ToCharArray().ForEach(x =>
            {
                val = val.Replace(x, ' ');
            });

            return val.Trim();
        }

    }
}