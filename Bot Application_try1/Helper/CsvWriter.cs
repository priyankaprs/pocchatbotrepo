﻿using Bot_Application_try1.Models;
using Spider.Common.Serialization;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace Bot_Application_try1.Helper
{
    public class CsvWriter
    {
        private const char C_DELIMITER = ';';
        private const char C_QUALIFIER = '"';
        protected readonly static CSVSerializer<ApplicantRecord> TargetCsvWriter = new CSVSerializer<ApplicantRecord>(false, C_DELIMITER, C_QUALIFIER);

        public static void WriteRecordsToFile(IEnumerable<ApplicantRecord> applicantRecord)
        {
            using (var writer = new StreamWriter(ConfigurationManager.AppSettings["OutputDirectory"] + "ApplicantLog" + ".csv", true))
            {
                TargetCsvWriter.SerializeArray(writer, applicantRecord, false);
            }
        }

    }
}