﻿using Bot_Application_try1.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace Bot_Application_try1.Helper
{
    public class CsvMapper
    {
        public static ApplicantRecord Map(ApplicantProfile applicantProfile) {
            return new ApplicantRecord()
            {
                Name = applicantProfile.ApplicantDetails.Name,
                ContactNumber = applicantProfile.ApplicantDetails.ContactNumber,
                EmailAddress = applicantProfile.ApplicantDetails.EmailAddress,
                NoticePeriod = applicantProfile.ApplicantDetails.NoticePeriod.ToString(),
                AreasOfExpertise = applicantProfile.ApplicantDetails.AreasOfExpertise.Select(i => i.ToString()).ToList(),
                PreferredLocation = applicantProfile.ApplicantDetails.PreferredLocation.ToString(),
                YearsOfExperience = applicantProfile.ApplicantDetails.YearsOfExperience,
                FileName = GetFileName(applicantProfile)
            };
        }

        private static string GetFileName(ApplicantProfile applicantProfile)
        {
            string extension = Path.GetExtension(applicantProfile.FileName);
            string applicantName = applicantProfile.ApplicantDetails.Name.Replace(' ', '_').Replace('.', '_');
            string phoneNumber = applicantProfile.ApplicantDetails.ContactNumber.Replace(' ', '_').Replace('.', '_').Replace("+",string.Empty).Trim();
            string filename = string.Concat(ConfigurationManager.AppSettings["OutputDirectory"],applicantName,'_',phoneNumber,extension);
            return filename;
        }
    }
}